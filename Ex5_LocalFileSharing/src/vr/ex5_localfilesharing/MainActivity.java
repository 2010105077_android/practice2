package vr.ex5_localfilesharing;

import java.io.*;
import java.util.*;

import android.content.*;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.*;
import android.support.v7.app.*;
import android.view.*;
import android.widget.*;

public class MainActivity extends ActionBarActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		Calendar cal = Calendar.getInstance();
		String example = cal.get(Calendar.YEAR) + "년" + cal.get(Calendar.MONTH) +
				"월" + cal.get(Calendar.DATE) + "일" + cal.get(Calendar.AM_PM) +
				"시" + cal.get(Calendar.MINUTE) + "분";
		byte[] buff = example.getBytes();
		File cacheDir = getCacheDir();
		File cacheFile = new File(cacheDir.getAbsolutePath(), "2010105077_이정진");
		try {
			FileOutputStream fos = new FileOutputStream(
					cacheFile.getAbsolutePath());
			fos.write(buff);
			fos.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void onButtonClick(View v) {
		Toast msg = null;
		TextView view = (TextView) findViewById(R.id.textView1);
		try {
			Context fileTest = createPackageContext(
					"vr.ex5_localfilesave",
					Context.CONTEXT_IGNORE_SECURITY);
			FileInputStream fis = fileTest.openFileInput("test1.txt");
			byte[] buff = new byte[fis.available()];
			while (fis.read(buff) != -1) {
				;
			}
			fis.close();
			view.setText(new String(buff));
		} catch (NameNotFoundException e) {
			msg = Toast.makeText(this, "파일 읽기 실패\n패키지를 찾을 수 없음",
					Toast.LENGTH_SHORT);
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			msg = Toast.makeText(this, "파일 읽기 실패\n파일을 찾을 수 없음",
					Toast.LENGTH_SHORT);
			e.printStackTrace();
		} catch (IOException e) {
			msg = Toast.makeText(this, "파일 읽기 실패\n내용을 읽을 수 없음",
					Toast.LENGTH_SHORT);
			e.printStackTrace();
		}
		if (msg == null) {
			msg = Toast.makeText(this, "파일 읽기 성공", Toast.LENGTH_SHORT);
		}
		msg.show();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
