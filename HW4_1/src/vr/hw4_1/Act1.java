package vr.hw4_1;

import android.content.*;
import android.os.*;
import android.support.v7.app.*;
import android.view.*;
import android.widget.*;

public class Act1 extends ActionBarActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

	}

	public void onClickButton(View v) {
		EditText edit1 = (EditText)findViewById(R.id.editText1);
		String name = edit1.getText().toString();
		EditText edit2 = (EditText)findViewById(R.id.editText2);
		String callnumber = edit2.getText().toString();
		
		Intent intent1 = new Intent(Act1.this, Act2.class);
		intent1.putExtra("name", name);
		intent1.putExtra("callnumber", callnumber);
		startActivityForResult(intent1, 1);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.act1, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
