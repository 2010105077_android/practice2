package vr.hw4_1;

import android.app.*;
import android.content.*;
import android.net.*;
import android.os.*;
import android.view.*;
import android.widget.*;

public class Act2 extends Activity {
	String name;
	String callnumber;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	
	    // TODO Auto-generated method stub
	    setContentView(R.layout.act2);
	    
	    name = getIntent().getStringExtra("name");
	    callnumber = getIntent().getStringExtra("callnumber");
	    if(name != null) {
	    	TextView txt = (TextView)findViewById(R.id.textView1);
	    	txt.setText("이름:"+name);
	    	}
	    if(callnumber != null) {
	    	TextView txt = (TextView)findViewById(R.id.textView2);
	    	txt.setText("전화번호:"+callnumber);
	    	}
	}
	
	
	public void onClickButton2(View v) {

		Intent intent1;

		intent1 = new Intent(Intent.ACTION_VIEW);
		intent1.setData(Uri.parse("tel:"+callnumber));
		startActivity(intent1);	
		}

}
